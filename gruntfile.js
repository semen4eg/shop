module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        concat: {
            // Для склеивания файлов
            dist: {
                src: [
                    'work/js/*.js', // Все js-файлы в директории js
                                    ],
                dest: 'product/js/glue.js',
            }
        },


        uglify: {
        build: {
            src: 'product/js/glue.js',
            dest: 'product/js/glue.min.js'// !!! с минифицированным не работает
        }
    },

    compress: {
      main: {
        options: {
          mode: 'gzip'
        },
        files: [
          // как-то так, лучше не придумал пока
          {expand: true, src: ['product/js/*.js'], ext: '.js.gz'},
          {expand: true, src: ['product/js/*.min.js'], ext: '.js.min.gz'}
        ]
      }
    }


    });

    // Плагин
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compress');
    // Задача по умолчанию, которая будет выполняться при запуске команды grunt
    grunt.registerTask('default', ['concat', 'uglify', 'compress']);

};
