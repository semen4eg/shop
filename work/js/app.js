'use strict';
/* Создаем приложение MyStore и лэйауты для страниц магазина и корзины */

var MyApp = angular.module('MyStore', ['ngRoute']).
  config(['$routeProvider', function($routeProvider) {
  $routeProvider.
      when('/store', {
        templateUrl: 'shopping_cart/store.htm',
        controller: MyController
      }).
      when('/cart', {
        templateUrl: 'shopping_cart/shoppingCart.htm',
        controller: MyController
      }).
      otherwise({
        redirectTo: '/store'
      });
}]);

/* Создаем сервис для обеспечения корзины и магазина */

MyApp.factory("MyService", function () {

    // магазин
    var myStore = new store();

    // корзина
    var myCart = new shoppingCart("MyStore");

    return {
        store: myStore,
        cart: myCart
    };
});
