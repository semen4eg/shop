
function store() {
    this.goods = [
        new product("goods1", "Яблоко", "Lorem ipsum dolor sit amet", 10),
        new product("goods2", "Перец", "Lorem ipsum dolor sit amet", 20),
        new product("goods3", "Чили", "Lorem ipsum dolor sit amet", 15),
        new product("goods4", "Томат", "Lorem ipsum dolor sit amet", 16),
        new product("goods5", "Курица", "Lorem ipsum dolor sit amet", 30),
        new product("goods6", "Арбуз", "Lorem ipsum dolor sit amet", 8),
        new product("goods7", "Батон", "Lorem ipsum dolor sit amet", 10),
        new product("goods8", "Пирожное", "Lorem ipsum dolor sit amet", 14),
        new product("goods9", "Креветка", "Lorem ipsum dolor sit amet", 25),
    ];

}

store.prototype.getProduct = function (sku) {
    for (var i = 0; i < this.goods.length; i++) {
        if (this.goods[i].sku == sku)
            return this.goods[i];
    }
    return null;
}
