
// пакеты, которые я использую
var gulp  = require('gulp');
// var uglify = require('gulp-uglify');
// var concat = require('gulp-concat');
var less = require('gulp-less');
var gutil = require('gulp-util');
var csso = require('gulp-csso');
var watch = require('gulp-watch');
var gzip = require('gulp-gzip');

gulp.task('default', ['watch']);
// gulp.task('minify', function () {
//     gulp.src('work/js/*.js') // берем все файлы из папки скриптов
//     .pipe(concat('glue.js')) // объединение
//     .pipe(uglify()) // минификация js
//     .pipe(gulp.dest('product/js')); // публикуем склеенный и минифицированный скрипт
// });

gulp.task('css', function () {
    return gulp.src('work/less/bootstrap.less')
    .pipe(less()) // сборка всех less в css
    .pipe(csso()) // минификация css
    .pipe(gulp.dest('product/css'))
    .pipe(gzip()) // gzip
    .pipe(gulp.dest('product/css'))
});

// вотчер
gulp.task('watch', function() {
  gulp.watch('work/less/*.less', ['css']);
});
